<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsAndCharacters extends Migration
{
    
    public function up(){
     
        if (Schema::hasTable('characters')) return;
        
        Schema::create('characters', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_marvel');

            $table->unique(["id_marvel"], 'characters_id_marvel_unique');
            $table->nullableTimestamps();
        });

        if (Schema::hasTable('teams')) return;
        Schema::create('teams', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('user_id')->unsigned();
            $table->integer('character_id')->unsigned();

            $table->index(["user_id"], 'users_teams_id_fk_idx');

            $table->index(["character_id"], 'characters_teams_id_fk_idx');
            $table->nullableTimestamps();


            $table->foreign('user_id', 'users_teams_id_fk_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('character_id', 'characters_teams_id_fk_idx')
                ->references('id')->on('characters')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
        Schema::dropIfExists('teams');
    }
}
